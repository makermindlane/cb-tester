use embedded_svc::{http::Headers, io::Read};

use esp_idf_svc::{
    eventloop::EspSystemEventLoop,
    hal::peripherals::Peripherals,
    http::{self, server::EspHttpServer, Method},
    io::Write,
    nvs::EspDefaultNvsPartition,
    wifi::{BlockingWifi, ClientConfiguration, Configuration, EspWifi},
};

use log::info;

fn main() -> anyhow::Result<()> {
    // It is necessary to call this function once. Otherwise some patches to the runtime
    // implemented by esp-idf-sys might not link properly. Se. https://github.com/esp-rs/esp-idf-template/issues/71
    esp_idf_svc::sys::link_patches();

    // Bind the log crate to the ESP Logging facilities
    esp_idf_svc::log::EspLogger::initialize_default();

    let p = Peripherals::take()?;
    let event_loop = EspSystemEventLoop::take()?;
    let nvs = EspDefaultNvsPartition::take()?;

    // Wifi
    let mut wifi = BlockingWifi::wrap(
        EspWifi::new(p.modem, event_loop.clone(), Some(nvs))?,
        event_loop,
    )?;
    let wifi_conf = Configuration::Client(ClientConfiguration {
        ssid: "Paheli 4G".try_into().unwrap(),
        password: "PASSpaheli4g@1".try_into().unwrap(),
        auth_method: esp_idf_svc::wifi::AuthMethod::WPA2Personal,
        ..Default::default()
    });
    wifi.set_configuration(&wifi_conf)?;
    wifi.start()?;
    wifi.connect()?;
    wifi.wait_netif_up()?;

    let ip_info = wifi.wifi().sta_netif().get_ip_info()?;
    info!("Info: {:?}", ip_info);

    // Server
    let server_conf = http::server::Configuration {
        stack_size: 10240,
        ..Default::default()
    };
    let mut server = EspHttpServer::new(&server_conf)?;

    server.fn_handler("/", Method::Get, |req| {
        let mut resp = req.into_ok_response()?;
        resp.write_all("Hello from the ESP side".as_bytes())
    })?;

    server.fn_handler::<anyhow::Error, _>("/exec_cmd", Method::Post, |mut req| {
        let len = req.content_len().unwrap_or(0) as usize;
        if len > 128 {
            req.into_status_response(413)?
                .write_all("Request too big".as_bytes())?;
            return Ok(());
        }

        let mut buf = vec![0; len];
        req.read_exact(&mut buf).unwrap();
        let mut resp = req.into_ok_response()?;
        info!("{:?}", buf);
        resp.write_all(buf.as_slice())?;

        Ok(())
    })?;

    core::mem::forget(wifi);
    core::mem::forget(server);

    info!("Server up and running");
    // std::thread::sleep(std::time::Duration::from_secs(5));

    Ok(())
}
